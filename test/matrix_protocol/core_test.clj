(ns matrix-protocol.core-test
  (:require [clojure.test :refer :all]
            [tick.core :as t]
            [tick.alpha.interval :as t]
            [matrix-protocol.core :refer :all]
            [matrix-protocol.helpers :refer :all]))

(defn make-payload [params]
  (-> {}
      (conj  [:headers
              (-> {}
                  (conj (when (:access_token params) {"Authorization" (str "Bearer" " " (:access_token params))}))
                  (conj (when (:content_type params) {"Content-Type" "image/jpeg"})))])
      (conj (when (:url-params params) [:url-params (:url-params params)]))))

(make-payload {:access_token "123" :content_type "image/jpeg"})
(make-payload {:access_token "123"})
(make-payload {})

(deftest build-payload-headers-test
  (testing "Test headers are added"
    (is (= (make-payload {:access_token "123" :content_type "image/jpeg"})
           {:headers {"Authorization" "Bearer 123", "Content-Type" "image/jpeg"}})))

  (testing "Test headers are not added"
    (is (= (make-payload {})
           {:headers {}}))))

(deftest mxc-media-test
  (testing "Test mxc url translations."
    (is (= (mxc->img-thumbnail-url "mxc://matrix.org/images")
           "https://matrix.org/_matrix/media/r0/thumbnail/matrix.org/images?width=200&height=200"))))

(deftest mxc-timestamp-test
  (testing "Test timestamp to date"
    (is (= (mxc-ts->format 1591780990289) "2020-Jun-10"))))

(deftest mxc-timestamp-compare-test
  (testing "Test timestamp to date"
    (is (= (mxc-ts-compare
            1591780990280
            1591780990288) :precedes))
    (is (= (mxc-ts-compare
            1591780990288
            1591780990280) :preceded-by))))

(deftest a-test
  (testing "FIXME, I fail."
    (is (= 0 1))))
