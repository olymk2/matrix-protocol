(ns matrix-protocol.mx-specs
  (:require [clojure.spec.alpha :as s]
            [clojure.test.check.generators :as gen]))

(def mx-charset (set "abcdefghijklmnopqrstuvwxyz0123456789._=-/"))

(s/def ::pos-int? (s/conformer seq #(apply str %)))
(s/def ::str->chars (s/conformer seq #(apply str %)))
(s/def :mx/chars mx-charset)
(s/def :mx/charset (s/* :mx/chars))

(defn gen-identifier2 []
  (gen/fmap identity
            (gen/vector (s/gen :mx/chars) (+ 1 (rand-int 12)))))

(s/def :mx/chars (set "abcdefghijklmnopqrstuvwxyz0123456789._=-/"))

(defn gen-identifier []
  (gen/fmap (partial apply str) 
            (gen/vector (s/gen :mx/chars) (+ 1 (rand-int 12)))))


;; TODO min / max length of user part @user-part: ?
(s/def :mx/identifier
  (s/spec (s/and string?
                 #(> (count %) 0)
                 ;; check ever charcter in string is in the character allowed set
                 #(every?  mx-charset (seq %))
                 (s/conformer seq #(apply str %)))
          :gen gen-identifier))



(s/def :matrix/version (s/spec string? :gen #(gen/return (str "r" (rand-int 100) "." (rand-int 100) "." (rand-int 100)))))

(s/def :matrix/versions (s/coll-of :matrix/version) )
(s/def :matrix/unstable_features (s/keys))
(s/def :matrix.event/type #{"m.room.message" "m.room.canonical_alias"})
(s/def ::mxc #{"mxc://"})
(s/def :matrix/timestamp pos-int?)

(defn gen-mxc-addr []
  (gen/fmap #(apply str %)
            (gen/tuple (gen/return (gen/generate (gen-identifier)))
                       (gen/return ":")
                       (gen/return "example.org"))))


(defn gen-mxc-room-alias []
  (gen/fmap #(apply str %)
            (gen/tuple (gen/return "#")
                       (gen/return (gen/generate (gen-identifier)))
                       (gen/return ":")
                       (gen/return "example.org"))))

(defn gen-mxc-room-id []
  (gen/fmap #(apply str %)
            (gen/tuple  (gen/return "!") (gen-mxc-addr))))


(defn gen-mxc-user-id []
  (gen/fmap
   #(apply str %)
   (gen/tuple
    (gen/return "@") (gen-mxc-addr))))

(defn gen-mxc-event-id []
  (gen/fmap
   #(apply str %)
   (gen/tuple
    (gen/return "$") (gen-mxc-addr))))

(defn gen-mxc-url []
  (gen/fmap
   #(apply str %)
   (gen/tuple
    (s/gen ::mxc)
    (s/gen string?)
    (gen/return "/")
    (s/gen string?))))

(def room-id-regex #"^\![a-z0-9\.\_\=\-\/]+:[a-z0-9\.\_\=\-\/]+")
(def room-alias-regex #"^\#[a-z0-9\.\_\=\-\/]+:[a-z0-9\.\_\=\-\/]+")
(def user-id-regex #"^\@[a-z0-9\.\_\=\-\/]+:[a-z0-9\.\_\=\-\/]+")
(def event-id-regex #"^\$[a-z0-9\.\_\=\-\/]+:[a-z0-9\.\_\=\-\/]+")

(s/def :matrix/mxc-uri (s/spec any? :gen gen-mxc-url))
(s/def :matrix.event/sender (s/spec (s/and string? #(re-matches user-id-regex %)) :gen gen-mxc-user-id))
(s/def :matrix.event/origin_server_ts pos-int?)
(s/def :matrix.event/unsigned (s/keys))
(s/def :matrix.event/content (s/keys))
(s/def :matrix.event/room_id (s/spec (s/and string? #(re-matches room-id-regex %))  :gen gen-mxc-room-id))
(s/def :matrix.event/room_alias (s/spec (s/and string? #(re-matches room-alias-regex %)) :gen gen-mxc-room-alias))
(s/def :matrix.event/event_id (s/spec (s/and string? #(re-matches event-id-regex %)) :gen gen-mxc-event-id))

(s/def :matrix/event (s/keys :req-un [:matrix.event/content
                                      :matrix.event/type
                                      :matrix.event/event_id
                                      :matrix.event/room_id
                                      :matrix.event/sender
                                      :matrix.event/origin_server_ts
                                      :matrix.event/unsigned]))

(s/def :matrix/events (s/coll-of :matrix/event))
(s/def :matrix/account_data (s/coll-of :matrix/event))
(s/def :matrix/to_device (s/coll-of :matrix/event))
(s/def :matrix/device_lists (s/keys))
(s/def :matrix/presence (s/keys))
(s/def :matrix/rooms (s/keys))
(s/def :matrix/groups (s/keys))
(s/def :matrix/device_one_time_keys_count (s/keys))
(s/def :matrix/next_batch string?)


(s/def :mx.response/version (s/keys :req-un [:matrix/versions]
                                    :opt:un [:matrix/unstable_features]))
(s/def :mx.response/sync (s/keys :req-un [:matrix/account_data
                                          :matrix/to_device
                                          :matrix/device_lists
                                          :matrix/presence
                                          :matrix/rooms
                                          :matrix/groups
                                          :matrix/device_one_time_keys_count
                                          :matrix/next_batch]
                                 :opt-un []))
;; {:account_data {:events []}, :to_device {:events []}, :device_lists {:changed [], :left []}, :presence {:events []}, :rooms {:join {}, :invite {}, :leave {}}, :groups {:join {}, :invite {}, :leave {}}, :device_one_time_keys_count {}, :next_batch "s1557491640_757284957_2333780_595510000_427195557_1553853_97754365_329360033_131055"}

(def gen-mx-event
  (fn [] (gen/generate (s/gen :matrix/event))))
;(gen-mx-event)
(comment
  (gen/generate (gen-identifier))
  (gen/generate (gen-identifier2))
  (gen/generate (gen-identifier))

  (s/valid? :matrix.event/room_id "!kjh:t.org")
  (s/valid? :matrix.event/room_id "!kjht.org")
  (gen/generate (gen-mxc-event-id))
  (gen/generate (gen-mxc-user-id))
  (gen/generate (s/gen :matrix/account_data ))
  (gen/generate (s/gen :mx.response/version))
  (gen/generate (s/gen :mx.response/sync))
  (gen/generate (s/gen :mx/chars))
  (gen/generate (s/gen :mx/user))
  (gen/sample (s/gen :mx/identifier))
  (gen/generate (s/gen :matrix/versions))

  (gen/sample (s/gen :matrix.event/event_id))
  (gen/sample (s/gen :matrix.event/room_id))
  (gen/sample (s/gen :matrix.event/sender))
  (gen/sample (s/gen :matrix.event/type))
  (gen/sample (s/gen :matrix.event/unsigned))

  (gen/sample (s/gen :mx/charset))
  (gen/sample (s/gen :mx/identifier))

  (gen/generate (s/gen :matrix.event/event_id))
  (gen/generate (s/gen :matrix.event/type))
  (gen/generate (s/gen :matrix.event/unsigned))
  (gen/generate (s/gen :matrix/user_id))
  (gen/generate (s/gen :matrix/room_id))
  (gen/generate  (gen-mxc-room-id))
  (gen/generate  (gen-mxc-room-alias))
  (gen/sample (s/gen (gen-mxc-room-alias)))
  (gen/generate (gen-mxc-room-alias))
  (gen/generate (s/gen :matrix/event_id))
  (gen/generate (s/gen :matrix/event))
  (gen/generate (s/gen :matrix/events))
  (gen/sample (s/gen :matrix/events))
  (gen/generate (s/gen :matrix/mxc))
  (gen/generate (s/gen :matrix/mxc-uri)))
