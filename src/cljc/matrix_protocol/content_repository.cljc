(ns matrix-protocol.content-repository
  (:require [clojure.core.async :as async]
            [matrix-protocol.helpers :as mh :refer [cb->async->fn2]]
            [matrix-protocol.core :as mc :refer [register-user login]]
            [matrix-protocol.core :refer [make-request]]
            [matrix-protocol.helpers :as mh]))

(defn upload
  "Register a new user or guest."
  ([payload form-data  handler]
   (make-request
    (merge
     {:uri (str mh/home-media-url "upload")
      :url-params {:filename (:filename payload) :access_token (:access_token payload)}
      :method :upload
      ;:format {:content-type "image/jpeg"}
      :body form-data}
     payload)
    handler)))


(defn upload-test [data params result]
     (async/go
       (reset! result
               (->
                ;; if we have an access key use it else try as guest.
                (async/<! (cb->async->fn2
                           (select-keys params [:username :password])

                           login
                           (fn [p r] (select-keys r [:access_token]))))

                #_(merge (select-keys params [:filename]))
                #_(async/<! (cb->async->fn2
                           upload
                           data
                           (fn [r] r)
                           ))

                ;; Merge access token with params, dissoc access_token so we don't replace
                ;(merge (dissoc params :access_token) {:dir "b" :limit limit})
                ;async/<!
                ))))

(comment
  (def result (atom {}))

  (upload  {:filename "image.jpeg" :access_token ""}
  (slurp "/tmp/image.jpg")
  #(prn %))

  (upload-test (slurp "/tmp/image.jpg") {:username "" :password "" :filename "image.jpeg"} result)
@result

;; curl -X POST \
;;      -H 'Content-Type: image/jpg' \
;;      -H 'Authorization: Bearer 123 \
;;      -d @/tmp/image.jpg 'https://matrix.org/_matrix/media/r0/upload'
