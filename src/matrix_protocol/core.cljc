(ns matrix-protocol.core
  (:require [ajax.core :refer [GET json-response-format POST PUT]]
            [matrix-protocol.helpers :as mh]))

(defn make-request
  "Simple wrapper around core async"
  [{:keys [uri method url-params form-params timeout]
    :or {timeout 0} :as params} handler]
  (prn (dissoc params :body))
  (case method
    :get (GET uri
           {:params url-params
            :response-format (json-response-format {:keywords? true})
            :handler handler})
    :post (POST uri
            {:url-params (merge url-params (select-keys form-params [:access_token]))
             :params (dissoc form-params :access_token)
             :response-format (json-response-format {:keywords? true})
             :format (or (:format params) :json)
             ;:error-handler #(prn %)
             :headers {"Content-Type" "image/jpeg"}
             :handler handler})
    :upload (POST uri
              {:url-params  (dissoc url-params :access_token)
               :body (:body params)
               :response-format (json-response-format {:keywords? true})
               :format :raw ;(or (:format params) :json)
             ;:error-handler #(prn %)
               :headers {"Content-Type" "image/jpg" "Authorization" (str "Bearer" " " (-> params :url-params :access_token))}
               :handler handler})

    :put (PUT uri
           {:url-params (merge url-params (select-keys form-params [:access_token]))
            :params (dissoc form-params :access_token)
            :response-format (json-response-format {:keywords? true})
            :format :json
            :handler handler})))

(defn consent
  ([{:keys [u h] :as payload} handler]
   (make-request
    (merge
     {:uri (str mh/home-server-url "consent")
      :method (if (some :u payload) :post :get)
      :params (merge payload {:v "1.0"})})
    handler)))

(defn login
  "Matrix login handler"
  ([handler]
   (make-request
    {:uri (str mh/home-server-url "login")
     :method :get}
    handler))
  ([{:keys [username password] :as extra} handler]
   (make-request
    {:uri (str mh/home-server-url "login")
     :method :post
     :form-params {:user username
                   :password password
                   :type (or (:login-type :extra) "m.login.password")}} handler)))

(defn register-user
  "Register a new user or guest."
  ([payload handler]
   (make-request
    (merge
     {:uri   (str mh/home-server-url "register")
      :url-params {:kind (:kind payload "user")}
      :method :post
      :form-params {}}
     payload)
    handler)))

(defn room-create
  "Create a new matrix room"
  ([{:keys [room access_token] :as payload} handler]
   (make-request
    (merge
     {:uri   (str mh/home-server-url "createRoom")
      :method :post}
     payload)
    handler)))

(defn room-join
  "Join an existing room"
  ([{:keys [room access_token] :as payload} handler]
   (make-request
    (merge
     {:uri   (str mh/home-server-url "rooms/" room "/join")
      :method :get}
     payload)
    handler)))

(defn room-state
  "Join an existing room"
  ([{:keys [room access_token] :as payload} handler]
   (make-request
    (merge
     {:uri   (str mh/home-server-url "rooms/" room "/state")
      :method :get}
     payload)
    handler)))

(defn room-redact-event
  "remove a room Event
  PUT /_matrix/client/api/r0/rooms/{roomId}/redact/{eventId}/{txnId} "
  [{:keys [room event-id] :as payload} handler]
  (make-request
   {:uri (str mh/home-server-url "rooms/" room "/redact/" event-id "/" (mh/mxc-txnid))
    :method :put
    :form-params (dissoc payload :room)}
   handler))

(defn room-event [{:keys [room event-id] :as payload} handler]
  (make-request
   (merge
    {:uri (str mh/home-server-url "rooms/" room "/event/" event-id)
     :method :get
     :url-params (dissoc payload :room :event-id)})
   handler))

(defn redact-message
  "remove a message just a wrapper around remove event.
  PUT /_matrix/client/api/r0/rooms/{roomId}/redact/{eventId}/{txnId} "
  [{:keys [room event-id txn-id] :as payload} handler]
  (room-redact-event payload handler))

(defn room-messages
  ([{:keys [room from dir limit access_token] :as payload} handler]
   (make-request
    (merge
     {:uri (str mh/home-server-url "rooms/" room "/messages")
      :method :get
      :url-params (dissoc payload :room)})
    handler)))

(defn send-message
  "Send a message to a room"
  [{:keys [body msgtype room access_token] :or {msgtype "m.text"} :as payload} handler]
  (make-request
   {:uri (str mh/home-server-url "rooms/" room "/send/m.room.message/" (mh/mxc-txnid))
    :method :put
    :form-params (dissoc payload :room)}
   handler))

(defn sync-events [{:keys [access_token] :as payload} handler]
  (make-request
   {:uri (str mh/home-server-url "sync")
    :method :get
    :url-params payload}
   handler))

(defn search
  ([payload handler]
   (make-request
    (merge
     {:uri (str mh/home-server-url "search")
      :method :post
      :form-params payload})
    handler)))
