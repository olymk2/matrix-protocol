(ns matrix-protocol.reiterate
  (:require    [clojure.core.async :as async]
               [matrix-protocol.helpers :as mh :refer [cb->async->fn2]]
               [matrix-protocol.core :as mc]))

#_(defn merge-chunks [chunk1 chunk2]
  (assoc chunk2 :chunk
         (concat (:chunk chunk1) (:chunk chunk2))))
       
(defn merge-chunks [chunk1 chunk2]
  {:start (or (:start chunk2) (:start chunk1))
   :end (or (:end chunk2) (:end chunk1))
   :chunk (concat (:chunk chunk1) (:chunk chunk2))})

(defn event-loop
  "repeatedly call function (sync by default) calling callback to process the response, "
  ([params] (event-loop params mc/sync-events 5000 (fn [params response] response)))
  ([params callback] (event-loop params mc/sync-events 5000 callback))
  ([params timeout callback] (event-loop params mc/sync-events timeout callback))
  ([params func timeout callback]
   (async/go
     (loop [r {}]
       (when (some? r)
         (async/<! (async/timeout timeout))
         (recur (merge r (async/<!
                          (mh/cb->async->fn2
                           params
                           func
                           (fn [response] (callback params response)))))))))))

;; todo unpack out the async stuff
(defn messages-until
  "Keep fetching until user test condition it matched"
  ([{:keys [access_token] :as params} test-fn]
   (messages-until params test-fn (fn [_ _ response] response)))
  ([params test-fn callback]
   (async/go
     (loop [r {} iteration-count 0]
       ;; always run first pass exit on no chunks or user condition reached
       (let [from (or (:end r) (:from r) (:from params))
             chunk (async/<!
                    (mh/cb->async->fn2
                     (merge {:dir "b" :limit 100}
                            (conj params (when from [:from from])))
                     (fn [params cb] (mc/room-messages params cb))
                     (fn [params iteration-count response] (callback params iteration-count response))))]
         (if (and  (seq (:chunk chunk)) (test-fn r iteration-count))
           (recur (merge-chunks r chunk)
                  (inc iteration-count))
           r))))))


