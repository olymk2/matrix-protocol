(ns matrix-protocol.helpers
  (:require
   [clojure.string :as s]
    [tick.core :as t]
   [tick.alpha.interval :as ti]
   [tick.locale-en-us]
   [tick.timezone]
   [clojure.core.async :as async]
   [nano-id.core :as nano :refer [nano-id]]))

(def schema "https://")
(def home-server "matrix.org")
(def home-server-url (str schema home-server "/_matrix/client/r0/"))
(def home-media-url (str schema home-server "/_matrix/media/r0/"))

(defn mxc-txnid [] (nano-id))
(defn is-image-event? [event] (= "m.image" (-> event :content :msgtype)))
(defn event-filter
  "Filter out events which match the supplied fn."
  [events func]
  (update events :chunk (fn [chunks] (filter (fn [chunk] (func chunk)) chunks))))
(defn mxc-ts-compare
  [ts1 ts2] (ti/relation (t/instant (t/new-duration ts1 :millis))
                        (t/instant (t/new-duration ts2 :millis))))

(defn mxc-ts->time-instant [ts]
   (t/instant (t/new-duration ts :millis)) )

(defn mxc-ts->time-instant-zoned
  ([ts] (mxc-ts->time-instant-zoned ts "Etc/UTC"))
  ([ts tz]
   (t/in (t/instant (t/new-duration ts :millis)) tz)))

(defn mxc-ts->format
  ([ts] (mxc-ts->format ts "yyyy-MMM-dd" "Etc/UTC"))
  ([ts fmt] (mxc-ts->format ts fmt "Etc/UTC"))
  ([ts fmt tz]
   (t/format (t/formatter fmt)  (t/in (t/instant (t/new-duration ts :millis)) tz))))

(defn build-media-url
  ([parts] (build-media-url parts "download"))
  ([{:keys [server-name media-id]} media-type]
   (str "https://" server-name "/_matrix/media/r0/" media-type "/" server-name "/" media-id)))

(defn mxc->url
  ([url] (mxc->url url "thumbnail"))
  ([url type]
   (clojure.string/replace
    (str url)
    "mxc://matrix.org/"
    (str "https://matrix.org/_matrix/media/r0/" type "/matrix.org/"))))

(defn mxc->file-url
  ([url] (mxc->file-url url "download"))
  ([url media-type]
   (build-media-url
    (zipmap [:server-name :media-id]
            (-> url
                (subs 6)
                (clojure.string/split #"/")))
    media-type)))


(defn mxc->img-thumbnail-url
  ([url] (mxc->img-thumbnail-url url 200 200))
  ([url width height] 
   (str (mxc->url url)
        "?width=" width "&height=" height)))


(defn cb->async
  "Invoke a function that you'd invoke with `(func params callback)` and return
  a core.async channel instead of the callback."
  [params func]
  (let [result (async/chan 1)]
    (func params (fn [fn-result] (async/go (async/>! result fn-result))))
    result))

(defn cb->async->fn1
  "Invoke a function that you'd invoke with `(func params callback)` and return
  a core.async channel instead of the callback."
  [params func func-cb]
  (let [result (async/chan 1)]
    (func params (fn [fn-result] (async/go (async/>! result (func-cb fn-result)))))
    result))

(defn cb->async->fn2
  "Invoke a function that you'd invoke with `(func params callback)` and return
  a core.async channel instead of the callback."
  [params func func-cb]
  (let [result (async/chan 1)]
    (func params (fn [fn-result] (async/go (async/>! result (func-cb params fn-result)))))
    result))

(comment (mxc->url "mxc://matrix.org/test")
         (mxc->file-url "mxc://matrix.org/test"))
